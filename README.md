Para que a aplicação execute com sucesso, é necessário ter o docker instalado e uma instância do redis em sua maquina.


1- Para executar a aplicação via container, é necessário ter o docker instalado em sua máquina, em seguida execute os comandos abaixo:

     1.1 - docker network create mynet -> cria uma network para comunição entre o container do redis e applicação.

     1.2 - docker run -d --name redis-server001 --network mynet -d -p 6378:6379 redis

     1.3 - docker run --network mynet -d -p 8080:8080 tioliveir/myrepo:api-teste-dasa

2- Para executar a aplicação em sua IDE, tenha uma versão do java 11+ instalado e siga os passos abaixo:

    2.1 - Entre no diretorio do projeto e execute o comando mvn clean install

    2.2 - Instale o plugin do lombok em sua IDE, para que não tenha problemas em reconhecer as anotações da lib.

    2.3 - Atenção!! é necessário ter uma instância do redis rodando em sua maquina de preferencia na porta 6378, ou configure no arquivo      application.propeties uma de sua preferencia. Caso já tem feito o passo 1.1 e 1.2 acima, ele já estará rodando.

Para acessar a API e documentação, acesse http://localhost:port/swagger-ui.html.
 

Para o deploy na aplicação na nuvem, foi utilizado a nuvem do Google Cloud, com uma instancia compute engine.
Para acessar a API na nuvem, acesse http://34.95.177.149/swagger-ui.html 
   
     


 


     


