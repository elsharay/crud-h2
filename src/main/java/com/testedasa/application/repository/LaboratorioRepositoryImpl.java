package com.testedasa.application.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.testedasa.application.model.Exame;
import com.testedasa.application.model.Laboratorio;

@Repository
public class LaboratorioRepositoryImpl implements LaboratorioRepository{

	private EntityManager entityManager;

	@Autowired
	public LaboratorioRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public Laboratorio save(Laboratorio entity) {
		entity.setId(null);
		entityManager.persist(entity);
		entityManager.flush();
		return entity;
	}

	@Override
	public void update(Laboratorio laboratorio) {
		entityManager.detach(laboratorio);
		entityManager.merge(laboratorio);
	}

	@Override
	public void delete(Laboratorio entity) {
		entityManager.remove(entity);	
	}

	@Override
	public List<Laboratorio> findByExame(String nome,Pageable pageable) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Laboratorio> criteriaQuery = criteriaBuilder.createQuery(Laboratorio.class);
		
		Root<Laboratorio> root = criteriaQuery.from(Laboratorio.class);
		Join<Laboratorio,Exame> joinExame = root.join("exames",JoinType.INNER); 
		
		Predicate predicate = criteriaBuilder.and(criteriaBuilder.equal(joinExame.get("nome"),nome));
		criteriaQuery = criteriaQuery.select(root).where(predicate);

		TypedQuery<Laboratorio> query = this.entityManager.createQuery(criteriaQuery);
		query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		query.setMaxResults(pageable.getPageSize());
		return query.getResultList();
	}

	@Override
	public List<Laboratorio> findAll(Pageable pageable) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Laboratorio> criteriaQuery = criteriaBuilder.createQuery(Laboratorio.class);

		Root<Laboratorio> root = criteriaQuery.from(Laboratorio.class);
		Predicate predicate = criteriaBuilder.and(criteriaBuilder.equal(root.<Boolean>get("status"),true));

		criteriaQuery = criteriaQuery.select(root).where(predicate);
		TypedQuery<Laboratorio> query = this.entityManager.createQuery(criteriaQuery);

		query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
		query.setMaxResults(pageable.getPageSize());
		query.setFlushMode(FlushModeType.COMMIT);
		return query.getResultList();
	}

	@Override
	public Optional<Laboratorio> findByIdAndStatus(Long id,Boolean status) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Laboratorio> criteriaQuery = criteriaBuilder.createQuery(Laboratorio.class);

		Root<Laboratorio> root = criteriaQuery.from(Laboratorio.class);
		Predicate predicate = criteriaBuilder.and(criteriaBuilder.equal(root.<Boolean>get("status"),true),criteriaBuilder.equal(root.<Long>get("id"),id));

		criteriaQuery = criteriaQuery.select(root).where(predicate);
		TypedQuery<Laboratorio> query = this.entityManager.createQuery(criteriaQuery);

		try {
			return Optional.of(query.getSingleResult());
		}catch (NoResultException exception){
			return Optional.ofNullable(null);
		}

	}

}
