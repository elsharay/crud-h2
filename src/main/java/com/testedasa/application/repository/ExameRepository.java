package com.testedasa.application.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.testedasa.application.model.Exame;

public interface ExameRepository extends JpaRepository<Exame, Long>{
	
	List<Exame> findByNome(String nome);

	List<Exame> findByStatus(Boolean status, Pageable pageable);

	Optional<Exame> findByIdAndStatus(Long id, Boolean status);
	

}
