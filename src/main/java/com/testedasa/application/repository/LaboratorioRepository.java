package com.testedasa.application.repository;

import java.util.List;
import java.util.Optional;

import com.testedasa.application.model.Laboratorio;
import org.springframework.data.domain.Pageable;

public interface LaboratorioRepository  {
	
	Laboratorio save(Laboratorio laboratorio);
	void update(Laboratorio laboratorio);
	void delete(Laboratorio laboratorio);
	Optional<Laboratorio> findByIdAndStatus(Long id,Boolean status);
	List<Laboratorio> findByExame(String nome,Pageable pageable);
	List<Laboratorio> findAll(Pageable pageable);
	

}
