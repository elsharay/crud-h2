package com.testedasa.application.exception;

public class MyBadRequestException  extends RuntimeException{

    public MyBadRequestException(String message){
        super(message);
    }
}
