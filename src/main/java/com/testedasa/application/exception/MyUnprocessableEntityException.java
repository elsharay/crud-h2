package com.testedasa.application.exception;


public class MyUnprocessableEntityException extends RuntimeException{

    public MyUnprocessableEntityException(String message){
        super(message);
    }
}
