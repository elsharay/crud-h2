package com.testedasa.application.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 *
 * @author Tiago Nery
 *
 */
public class WebSecurityCorsFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        HttpServletRequest req  = (HttpServletRequest) request;
        res.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        res.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, "POST,GET,GET,PATCH,TRACE,OPTIONS,PUT,HEAD");
        res.setHeader(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "3600");
        res.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS,"Authorization, Content-Type, Accept, Cache-Control,Origin, X-Requested-With,X-Custom-Header");
        res.setHeader(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE);

        if (!req.getMethod().equals("OPTIONS")) {
            chain.doFilter(req, res);
        } else {
            ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_OK);
        }
    }

    @Override
    public void destroy() {


    }

}
