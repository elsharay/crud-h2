package com.testedasa.application.util;

import com.testedasa.application.dto.LaboratorioResponseDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import com.testedasa.application.dto.LaboratorioDTO;
import com.testedasa.application.model.Laboratorio;

@Mapper
public interface LaboratorioMapper {
	
	LaboratorioMapper INSTANCE = Mappers.getMapper(LaboratorioMapper.class);

	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateLaboratorioFromLaboratorioDto(LaboratorioDTO LaboratorioDto,@MappingTarget Laboratorio Laboratorio);

	void updateLaboratorioDtoFromLaboratorio(Laboratorio Laboratorio,@MappingTarget LaboratorioResponseDTO LaboratorioDto);

}
