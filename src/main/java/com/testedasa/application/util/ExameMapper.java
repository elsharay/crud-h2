package com.testedasa.application.util;

import com.testedasa.application.dto.ExameResponseDTO;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import com.testedasa.application.dto.ExameDTO;
import com.testedasa.application.model.Exame;

@Mapper
public interface ExameMapper {
	
	ExameMapper INSTANCE = Mappers.getMapper(ExameMapper.class);

	@BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
	void updateExameFromExameDto(ExameDTO exameDto,@MappingTarget Exame exame);

	void updateExameDtoFromExame(Exame exame, @MappingTarget ExameResponseDTO exameDto);

}
