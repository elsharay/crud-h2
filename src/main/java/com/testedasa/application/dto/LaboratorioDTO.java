package com.testedasa.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@AllArgsConstructor
public class LaboratorioDTO {
	
	private Long id;
	
	@NotBlank(message = "Nome é obrigatório")
	private String nome;
	
	@NotBlank(message = "Endereco não pode ser vazio")
	private String endereco;

	@Override
	public String toString() {
		return ""+id;
	}
}
