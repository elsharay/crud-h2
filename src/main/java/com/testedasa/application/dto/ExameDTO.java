package com.testedasa.application.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.testedasa.application.model.TipoExame;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExameDTO {
	
	private Long id;

	@NotBlank(message = "Campo nome, não pode ser vazio")
	@NotNull(message = "Campo nome, não pode ser nulo")
	private String nome;
	@NotNull(message = "Campo tipo, não pode ser nulo")
	private TipoExame tipo;

	


}
