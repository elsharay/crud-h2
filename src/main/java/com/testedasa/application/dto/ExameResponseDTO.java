package com.testedasa.application.dto;

import lombok.Getter;
import lombok.Setter;


import com.testedasa.application.model.TipoExame;

@Getter
@Setter
public class ExameResponseDTO {

	private Long id;
	private String nome;
	private TipoExame tipo;
	private Boolean status;


}
