package com.testedasa.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LaboratorioResponseDTO {

	private Long id;
	private String nome;
	private String endereco;
	private Boolean status;


}
