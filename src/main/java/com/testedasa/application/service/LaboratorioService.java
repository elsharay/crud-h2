package com.testedasa.application.service;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.testedasa.application.exception.MyUnprocessableEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.testedasa.application.dto.LaboratorioDTO;
import com.testedasa.application.model.Exame;
import com.testedasa.application.model.Laboratorio;
import com.testedasa.application.repository.ExameRepository;
import com.testedasa.application.repository.LaboratorioRepository;
import com.testedasa.application.util.LaboratorioMapper;

@Transactional
@Service
public class LaboratorioService {

    private LaboratorioRepository laboratorioRepository;
    private ExameRepository exameRepository;

    @Autowired
    public LaboratorioService(LaboratorioRepository laboratorioRepository, ExameRepository exameRepository) {
        this.laboratorioRepository = laboratorioRepository;
        this.exameRepository = exameRepository;
    }

    @CacheEvict(cacheNames = "Laboratorio", allEntries = true)
    public Laboratorio save(LaboratorioDTO laboratorioDTO) {
        Laboratorio laboratorio = new Laboratorio(true);
        LaboratorioMapper.INSTANCE.updateLaboratorioFromLaboratorioDto(laboratorioDTO, laboratorio);
        return laboratorioRepository.save(laboratorio);
    }

    @CacheEvict(cacheNames = "Laboratorio", allEntries = true)
    public Collection<Laboratorio> saveAll(Collection<LaboratorioDTO> laboratoriosDTO) {
        List<Laboratorio> laboratorios = new ArrayList<>();
        laboratoriosDTO.forEach(laboratorioDTO -> {
            Laboratorio laboratorio = new Laboratorio(true);
            LaboratorioMapper.INSTANCE.updateLaboratorioFromLaboratorioDto(laboratorioDTO, laboratorio);
            laboratorios.add(laboratorioRepository.save(laboratorio));
        });
        return laboratorios;
    }

    @CacheEvict(cacheNames = "Laboratorio", allEntries = true)
    public void updateAll(Collection<LaboratorioDTO> laboratoriosDTO) {
        List<Laboratorio> laboratorios = new ArrayList<>();
        laboratoriosDTO.forEach(laboratorioDTO -> {
            Optional<Laboratorio> optional = laboratorioRepository.findByIdAndStatus(laboratorioDTO.getId(), true);
            if (optional.isEmpty()) {
                throw new MyUnprocessableEntityException(
                        String.format("Laboratório com id %s não encontrado", laboratorioDTO.getId()));
            }
            Laboratorio laboratorio = optional.get();
            LaboratorioMapper.INSTANCE.updateLaboratorioFromLaboratorioDto(laboratorioDTO, laboratorio);
            laboratorioRepository.update(laboratorio);
        });

    }

    @Caching(evict = {
            @CacheEvict(value = "Laboratorio", key = "#laboratorioDTO.getId()")},
            put = {
                    @CachePut(cacheNames = "Laboratorio", key = "#laboratorioDTO.getId()")})
    public void update(final LaboratorioDTO laboratorioDTO) {
        Optional<Laboratorio> optional = laboratorioRepository.findByIdAndStatus(laboratorioDTO.getId(), true);
        if (optional.isEmpty()) {
            throw new MyUnprocessableEntityException(
                    String.format("Laboratório com id %s não encontrado", laboratorioDTO.getId()));
        }
        Laboratorio laboratorio = optional.get();
        LaboratorioMapper.INSTANCE.updateLaboratorioFromLaboratorioDto(laboratorioDTO, laboratorio);
        laboratorioRepository.update(laboratorio);
    }

    @Caching(evict = {
            @CacheEvict(value = "Laboratorio", key = "#id")},
            put = {
                    @CachePut(cacheNames = "Laboratorio", key = "#id")})
    public void remove(Long id) {
        Optional<Laboratorio> optional = laboratorioRepository.findByIdAndStatus(id, true);
        if (optional.isEmpty()) {
            throw new MyUnprocessableEntityException(String.format("laboratorio com id %s não encontrado", id));
        }
        optional.get().setStatus(false);
        laboratorioRepository.update(optional.get());
    }

    @CacheEvict(cacheNames = "Laboratorio", allEntries = true)
    public void removeAll(Collection<Long> ids) {
        ids.forEach(id -> {
            Optional<Laboratorio> optional = laboratorioRepository.findByIdAndStatus(id, true);
            if (optional.isEmpty()) {
                throw new MyUnprocessableEntityException(String.format("laboratorio com id %s não encontrado", id));
            }
            optional.get().setStatus(false);
            laboratorioRepository.update(optional.get());
        });
    }

    @Cacheable(cacheNames = "Laboratorio", key = "#id")
    public Laboratorio findById(Long id) {
        Optional<Laboratorio> optional = laboratorioRepository.findByIdAndStatus(id, true);

        if (optional.isEmpty()) {
            throw new MyUnprocessableEntityException(String.format("laboratorio com id %s não encontrado", id));
        }

        return optional.get();
    }

    @Cacheable(cacheNames = "Laboratorio", key = "#p0")
    public Collection<Laboratorio> findAll(Pageable pageable) {
        return laboratorioRepository.findAll(pageable);
    }

    @Cacheable(cacheNames = "Laboratorio", key = "#nome")
    public Collection<Laboratorio> findByExame(String nome, Pageable pageable) {
        return laboratorioRepository.findByExame(nome, pageable);
    }

    @Caching(evict = {
            @CacheEvict(value = "Laboratorio", key = "#labId")},
            put = {
                    @CachePut(cacheNames = "Laboratorio", key = "#labId")})
    public void removeExameFromLaboratorio(Long exameId, Long labId) {
        Optional<Exame> opExame = exameRepository.findByIdAndStatus(exameId, Boolean.TRUE);
        if (opExame.isEmpty()) {
            throw new MyUnprocessableEntityException(String.format("Exame com id %s não encontrado", exameId));
        }
        Exame exame = opExame.get();
        Optional<Laboratorio> opLaboratorio = laboratorioRepository.findByIdAndStatus(labId, true);
        if (opLaboratorio.isEmpty()) {
            throw new MyUnprocessableEntityException(String.format("Laboratório com id %s não encontrado", labId));
        }
        Laboratorio laboratorio = opLaboratorio.get();

        boolean match = laboratorio.getExames().stream().anyMatch(exame1 -> {
            return exame1.getId().equals(exameId);
        });

        if (!match) {
            throw new MyUnprocessableEntityException(String.format("Exame com id %s não está vinculado a este laboratório", exameId));
        }

        laboratorio.getExames().remove(exame);
        laboratorioRepository.update(laboratorio);
    }

    @Caching(evict = {
            @CacheEvict(value = "Laboratorio", key = "#labId")},
            put = {
                    @CachePut(cacheNames = "Laboratorio", key = "#labId")})
    public void addExameToLaboratorio(Long exameId, Long labId) {
        Optional<Exame> opExame = exameRepository.findByIdAndStatus(exameId, Boolean.TRUE);
        if (opExame.isEmpty()) {
            throw new IllegalArgumentException(String.format("Exame com id %s não encontrado", exameId));
        }
        Exame exame = opExame.get();
        Optional<Laboratorio> opLaboratorio = laboratorioRepository.findByIdAndStatus(labId, true);
        if (opLaboratorio.isEmpty()) {
            throw new MyUnprocessableEntityException(String.format("Laboratório com id %s não encontrado", labId));
        }

        Laboratorio laboratorio = opLaboratorio.get();
        boolean match = laboratorio.getExames().stream().anyMatch(exame1 -> {
            return exame1.getId().equals(exameId);
        });
        if (match) {
            throw new MyUnprocessableEntityException(String.format("Exame com id %s já foi adicionado a este laboratório", exameId));
        }
        laboratorio.getExames().add(exame);
        laboratorioRepository.update(laboratorio);
    }

}
