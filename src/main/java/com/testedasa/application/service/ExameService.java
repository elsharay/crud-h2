package com.testedasa.application.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.testedasa.application.exception.MyUnprocessableEntityException;
import com.testedasa.application.model.Exame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.testedasa.application.dto.ExameDTO;
import com.testedasa.application.repository.ExameRepository;
import com.testedasa.application.util.ExameMapper;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ExameService {

	private ExameRepository exameRepository;

	@Autowired
	public ExameService(ExameRepository exameRepository) {
		this.exameRepository = exameRepository;
	}

	@CacheEvict(cacheNames = "Exame", allEntries = true)
	public Exame save(ExameDTO ExameDTO) {
		Exame exame = new Exame(true);
		ExameMapper.INSTANCE.updateExameFromExameDto(ExameDTO, exame);
		exame.setId(null);
		return exameRepository.save(exame);
	}

	@CacheEvict(cacheNames = "Exame", allEntries = true)
	public Collection<Exame> saveAll(Collection<ExameDTO> examesDTO) {
		List<Exame> exames = new ArrayList<>();
		
		examesDTO.forEach(exameDTO -> {
			Exame exame = new Exame(true);
			ExameMapper.INSTANCE.updateExameFromExameDto(exameDTO, exame);
			exame.setId(null);
			exames.add(exameRepository.save(exame));
		});

		return exames;
	}

	@CacheEvict(cacheNames = "Exame", allEntries = true)
	public void updateAll(Collection<ExameDTO> examesDTO) {
		List<Exame> Exames = new ArrayList<>();
		examesDTO.forEach(exameDTO -> {
			Optional<Exame> optional = exameRepository.findByIdAndStatus(exameDTO.getId(),true);

			if (optional.isEmpty()) {
				throw new MyUnprocessableEntityException(
						String.format("Exame com id %s não encontrado", exameDTO.getId()));
			}

			Exame Exame = optional.get();
			ExameMapper.INSTANCE.updateExameFromExameDto(exameDTO, Exame);
			exameRepository.save(Exame);
		});
	}

	@Caching(evict = {
			@CacheEvict(value="Exame", key="#exameDTO.getId()") },
			put = {
					@CachePut(cacheNames = "Exame", key="#exameDTO.getId()")})
	public void update(ExameDTO exameDTO) {
		Optional<Exame> optional = exameRepository.findById(exameDTO.getId());

		if (optional.isEmpty()) {
			throw new MyUnprocessableEntityException(
					String.format("Exame com id %s não encontrado", exameDTO.getId()));
		}

		Exame Exame = optional.get();
		ExameMapper.INSTANCE.updateExameFromExameDto(exameDTO, Exame);
		exameRepository.save(Exame);
	}


	@Caching(evict = {
			@CacheEvict(value="Exame", key="#id") },
			put = {
					@CachePut(cacheNames = "Exame", key="#id")})
	public  void remove(Long id) {
		Optional<Exame> optional = exameRepository.findByIdAndStatus(id,true);
		if (optional.isPresent()) {
			optional.get().setStatus(false);
			exameRepository.save(optional.get());
		}
	}

	@CacheEvict(cacheNames = "Exame", allEntries = true)
	public void removeAll(Collection<Long> ids) {
		ids.forEach(id -> {
			Optional<Exame> optional = exameRepository.findByIdAndStatus(id,true);
			if (optional.isPresent()) {
				optional.get().setStatus(false);
				exameRepository.save(optional.get());
			}
		});
	}

	@Cacheable(cacheNames = "Exame", key="#p0")
	public Collection<Exame> findAll(Pageable pageable) {
		return exameRepository.findByStatus(true,pageable);
	}

	@Cacheable(cacheNames = "Exame", key="#id")
	public Exame findById(Long id){
		Optional<Exame> optional = exameRepository.findById(id);
		if(optional.isEmpty()){
			throw new MyUnprocessableEntityException("Exame não encontrado.");
		}
		return optional.get();
	}

}
