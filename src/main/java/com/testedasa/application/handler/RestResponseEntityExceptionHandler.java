package com.testedasa.application.handler;

import com.testedasa.application.exception.MyBadRequestException;
import com.testedasa.application.exception.MyUnprocessableEntityException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.NoResultException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private final String errorBodyJSON = "{ \"error\": \"%s\" }";
    private final String errorBodyXML = "<error>\"%s\"</error>";

    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = { MyUnprocessableEntityException.class })
    protected ResponseEntity<Object> handleExceptionAsNotFound(MyUnprocessableEntityException ex, WebRequest request) {
        return this.handleExceptionInternal(ex, this.createResponseBody(ex, request), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
    }

    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = { NoResultException.class })
    protected ResponseEntity<Object> handleExceptionAsNotFound(NoResultException ex, WebRequest request) {
        return this.handleExceptionInternal(ex, this.createResponseBody(ex, request), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = { MyBadRequestException.class })
    protected ResponseEntity<Object> handleExceptionMyBadRequest(MyBadRequestException ex, WebRequest request) {
        return this.handleExceptionInternal(ex, this.createResponseBody(ex, request), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = { DataIntegrityViolationException.class })
    protected ResponseEntity<Object> handleExceptionMyBadRequest(DataIntegrityViolationException ex, WebRequest request) {
        return this.handleExceptionInternal(ex, this.createResponseBody(ex, request), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {

        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ResponseEntity(errors, HttpStatus.BAD_REQUEST);
    }

    private String createResponseBody(Exception ex, WebRequest request) {
        ex.printStackTrace();

        String responseBody = ex.getMessage() != null ? ex.getMessage() : ex.getClass().getName();
        String accept = request.getHeader(HttpHeaders.ACCEPT);

        if (accept != null && !accept.trim().isEmpty()) {
            if (accept.toLowerCase().contains(MediaType.APPLICATION_JSON_VALUE.toLowerCase()) || accept.toLowerCase().contains(MediaType.APPLICATION_JSON_UTF8_VALUE.toLowerCase())) {
                responseBody = String.format(this.errorBodyJSON, responseBody);
            }
            else if (accept.toLowerCase().contains(MediaType.APPLICATION_XML_VALUE.toLowerCase())) {
                responseBody = String.format(this.errorBodyXML, responseBody);
            }
        }

        return responseBody;
    }

    private class ErrorResponse
    {
        public ErrorResponse(String message, List<String> details) {
            super();
            this.message = message;
            this.details = details;
        }
        private String message;
        private List<String> details;


    }

}
