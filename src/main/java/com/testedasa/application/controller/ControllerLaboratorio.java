package com.testedasa.application.controller;

import com.testedasa.application.dto.LaboratorioDTO;
import com.testedasa.application.dto.LaboratorioResponseDTO;
import com.testedasa.application.model.Laboratorio;
import com.testedasa.application.service.LaboratorioService;
import com.testedasa.application.util.LaboratorioMapper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("laboratorio")
public class ControllerLaboratorio {

    
    private LaboratorioService service;
    
    @Autowired
    public ControllerLaboratorio(LaboratorioService service){
            this.LaboratorioService service = service;
    }


    @ApiOperation(value = "Cria um novo laboratório", notes = "Este método cria um novo laboratório e gerar um id automaticamente")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso criado"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public Laboratorio save(@RequestBody LaboratorioDTO laboratorioDTO) {
        return service.save(laboratorioDTO);
    }



    @ApiOperation(value = "Criar laboratórios", notes = "Este método cria um lote de laboratórios e gera seus ids automaticamente")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso criado"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/many")
    public Collection<Laboratorio> saveAll(@RequestBody Collection<LaboratorioDTO> laboratoriosDTO) {
        return service.saveAll(laboratoriosDTO);
    }

    @ApiOperation(value = "Atualiza um laboratório", notes = "Este método atualiza od dados de um laboratório")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Recurso atualizado"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @PutMapping("/{id}")
    public void update(@RequestBody LaboratorioDTO laboratorioDTO, @PathVariable("id") Long id) {
        laboratorioDTO.setId(id);
        service.update(laboratorioDTO);
    }

    @ApiOperation(value = "Atualizar  laboratórios", notes = "Este método atualiza laboratórios em lote")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Recurso atualizado"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @PutMapping("/many")
    public void updateAll(@RequestBody Collection<LaboratorioDTO> laboratoriosDTO) {
        service.updateAll(laboratoriosDTO);
    }

    @ApiOperation(value = "remove um laboratório", notes = "Este método remove um laboratório logicamente")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Recurso removido"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),})
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        service.remove(id);
    }

    @ApiOperation(value = "remove  laboratórios",
            notes = "Este método remove um lote de laboratórios logicamente")
    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Recurso atualizado"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),})
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping("/many")
    public void deleteAll(@RequestParam("id") Collection<Long> ids) {
        service.removeAll(ids);
    }

    @ApiOperation(value = "Retorna um laboratório",
            notes = "Este método retorna um laboratório")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um objeto"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),})
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}")
    public LaboratorioResponseDTO findById(@PathVariable("id") Long id) {
          return toLaboratorioResponseDTO(service.findById(id));
    }


    @ApiOperation(value = "Listar laboratórios", notes = "Este método retorna uma lista de laboratórios")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna lista de objetos"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),})
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Página de resultados que você deseja recuperar", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Número de informações por página.", defaultValue = "10")})
    public Collection<LaboratorioResponseDTO> findAll(
            @ApiIgnore
            @PageableDefault(page = 0, size = 10) Pageable pageable) {

        Collection<Laboratorio> laboratorios = service.findAll(pageable);
        Collection<LaboratorioResponseDTO> laboratorioDTOS = new ArrayList<>();

        laboratorios.forEach(laboratorio -> {
            laboratorioDTOS.add(toLaboratorioResponseDTO(laboratorio));
        });
        return laboratorioDTOS;
    }


    @ApiOperation(value = "Listar laboratório por exame",
            notes = "Este método método retorna uma lista de laboratórios de um exame")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna lista de objetos"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/exame/{nome}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Página de resultados que você deseja recuperar", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Número de informações por página.", defaultValue = "10")
    })
    public Collection<LaboratorioResponseDTO> findByExame(@ApiIgnore
            @PageableDefault(page = 0, size = 10) Pageable pageable,
            @PathVariable("nome") String nome) {

        Collection<Laboratorio> laboratorios = service.findByExame(nome, pageable);
        Collection<LaboratorioResponseDTO> laboratorioDTOS = new ArrayList<>();

        laboratorios.forEach(laboratorio -> {
              laboratorioDTOS.add(toLaboratorioResponseDTO(laboratorio));
        });
        return laboratorioDTOS;
    }


    @ApiOperation(value = "Vincular laboratório", notes = "Este método vincula um laboratório a um exame")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Recurso criado"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping("/{laboratorio_id}/exame/{exame_id}/add")
    public void addExameToLaboratorio(@PathVariable("exame_id") Long exameId,
                                      @PathVariable("laboratorio_id") Long laboratorioId) {
        service.addExameToLaboratorio(exameId, laboratorioId);
    }


    @ApiOperation(value = "Desvincular laboratório", notes = "Este método desvincula um laboratório de um exame")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Recurso removido"),
            @ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
            @ApiResponse(code = 422, message = "Erro de negocio"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    @DeleteMapping("/{laboratorio_id}/exame/{exame_id}/remove")
    public void removeExameFromLaboratorio(@PathVariable("exame_id") Long exameId,
                                           @PathVariable("laboratorio_id") Long laboratorioId) {
        service.removeExameFromLaboratorio(exameId, laboratorioId);
    }


    private LaboratorioResponseDTO toLaboratorioResponseDTO(Laboratorio laboratorio){
        LaboratorioResponseDTO laboratorioResponseDTO = new LaboratorioResponseDTO();
        LaboratorioMapper.INSTANCE.updateLaboratorioDtoFromLaboratorio(laboratorio, laboratorioResponseDTO);
        return laboratorioResponseDTO;
    }

}
