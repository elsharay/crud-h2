package com.testedasa.application.controller;

import com.testedasa.application.dto.ExameDTO;
import com.testedasa.application.dto.ExameResponseDTO;
import com.testedasa.application.model.Exame;
import com.testedasa.application.service.ExameService;
import com.testedasa.application.util.ExameMapper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("exame")
public class ControllerExame {
	
	private ExameService service;
	
	
	@Autowired
	public ControllerExame(ExameService service){
		this.service = service
	}


	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Recurso criado"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ApiOperation(value = "Criar exame",
			notes = "Este método cria um novo exame e gerar um id automaticamente")
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping
	public Exame save(@RequestBody @Valid ExameDTO exameDTO) {
		return service.save(exameDTO);
	}

	@ApiOperation(value = "Criar exames",
			notes = "Este método cria um lote de exames e gera seus ids automaticamente")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Recurso criado"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/many")
	public Collection<Exame> saveAll(@RequestBody  Collection<ExameDTO> examesDTO) {
		return service.saveAll(examesDTO);
	}


	@ApiOperation(value = "Atualiza um exame",
			notes = "Este método atualiza os dados de um exame")
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Recurso atualizado"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	@PutMapping ("/{id}")
	public void update(@RequestBody ExameDTO exameDTO,@PathVariable("id") Long id) {
		 exameDTO.setId(id);
		 service.update(exameDTO);
	}


	@ApiOperation(value = "Atualizar exames",
			notes = "Este método atualiza exames em lote")
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Recurso atualizado"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	@PutMapping ("/many")
	public void updateAll(@RequestBody Collection<ExameDTO> examesDTO) {
		service.updateAll(examesDTO);
	}

	@ApiOperation(value = "Remove um exame",
			notes = "Este método remove um exame logicamente")
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Recurso removido"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	@DeleteMapping ("/{id}")
	public void delete(@PathVariable("id") Long id) {
		service.remove(id);
	}

	@ApiOperation(value = "Remove exames",
			notes = "Este método remove exames em lote logicamente")
	@ApiResponses(value = {
			@ApiResponse(code = 202, message = "Recursos removidos"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	@DeleteMapping ("/many")
	public void deleteAll(@RequestParam("id") Collection<Long> ids) {
		service.removeAll(ids);
	}


	@ApiOperation(value = "Listar exames",
			notes = "Este método retorna uma lista de exames")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna lista de objetos"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping
	@ApiImplicitParams({
			@ApiImplicitParam(name = "página", dataType = "integer", paramType = "query",
					value = "Página de resultados que você deseja recuperar", defaultValue = "0"),
			@ApiImplicitParam(name = "quatidade", dataType = "integer", paramType = "query",
					value = "Número de informações por página.", defaultValue = "10")
	})
	public Collection<ExameResponseDTO> findAll(@ApiIgnore @PageableDefault(page = 0, size = 10) Pageable pageable) {

		Collection<Exame> exames = service.findAll(pageable);
		Collection<ExameResponseDTO> exameDTOS = new ArrayList<>();

		exames.forEach(exame -> {
			exameDTOS.add(toExameResponseDTO(exame));
		});
		return exameDTOS;
	}


	@ApiOperation(value = "Retorna um exame",
			notes = "Este método retorna um exame")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Retorna lista de objetos"),
			@ApiResponse(code = 400, message = "Entrada não atende ao dominio"),
			@ApiResponse(code = 422, message = "Erro de negocio"),
			@ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/{id}")
	public ExameResponseDTO findById(@PathVariable("id") Long id) {
		return toExameResponseDTO(service.findById(id));
	}

	private ExameResponseDTO toExameResponseDTO(Exame exame){
		ExameResponseDTO exameResponseDTO = new ExameResponseDTO();
		ExameMapper.INSTANCE.updateExameDtoFromExame(exame, exameResponseDTO);
		return exameResponseDTO;
	}

}
