package com.testedasa.application.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Entity
@AllArgsConstructor
public class Laboratorio implements Serializable {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "laboratorio_id")
	private Long id;

	@Column(length = 200,nullable = false)
	private String nome;

	@Column(length = 200,nullable = false)
	private String endereco;
	
	@Column(nullable = false)
	private Boolean status;
	
	@ManyToMany
	@JoinTable(
	  name = "lab_exame", 
	  joinColumns = @JoinColumn(name = "laboratorio_id"), 
	  inverseJoinColumns = @JoinColumn(name = "exame_id"))
	@JsonIgnore
	private Set<Exame> exames;
	
	public Laboratorio() {
		super();
	}

	public Laboratorio(Boolean status) {
		super();
		this.status = status;
	}

	@Override
	public String toString() {
		return "Laboratorio{" +
				"nome='" + nome + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Laboratorio that = (Laboratorio) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
