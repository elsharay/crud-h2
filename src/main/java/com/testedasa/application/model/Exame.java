package com.testedasa.application.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;


import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;

@Getter
@Setter
@Entity
public class Exame implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "exame_id" )
	private Long id;
	
	@Column(length = 200,nullable = false)
	private String nome;
	
	@Column(nullable = false)
	private Boolean status;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private TipoExame tipo;
	
	@ManyToMany(mappedBy = "exames")
	@JsonIgnore
    private Set<Laboratorio> laboratorios;
	
	
	public Exame() {
		super();
	}
	public Exame(Boolean status) {
		super();
		this.status = status;
	}
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Exame other = (Exame) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Exame [nome=" + nome + "]";
	}
	
	
}
