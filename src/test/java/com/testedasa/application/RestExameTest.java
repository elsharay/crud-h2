package com.testedasa.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.testedasa.application.dto.ExameDTO;
import com.testedasa.application.model.TipoExame;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class RestExameTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    public void shouldFecthAll() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/exame/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


    @Test
    public void shouldInsertOn() throws Exception {
        String json = objectMapper.writeValueAsString(new ExameDTO(null,"teste",TipoExame.ANALISE_CLINICA));
        mockMvc.perform(MockMvcRequestBuilders.post("/exame/").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated());
    }

    public void shouldInsertMany() throws Exception {
        List<ExameDTO> exames = new ArrayList<>();
        exames.add(new ExameDTO(null,"teste", TipoExame.ANALISE_CLINICA));
        exames.add(new ExameDTO(null,"teste1",TipoExame.ANALISE_CLINICA));
        exames.add(new ExameDTO(null,"teste1",TipoExame.ANALISE_CLINICA));

        String json = objectMapper.writeValueAsString(exames);
        mockMvc.perform(MockMvcRequestBuilders.post("/exame/").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldUpdate() throws Exception {
        String json = objectMapper.writeValueAsString(new ExameDTO(1l,"dasa",TipoExame.ANALISE_CLINICA));
        mockMvc.perform(MockMvcRequestBuilders.put("/exame/1").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isAccepted());
    }

    @Test
    public void shouldUpdateMany() throws Exception {
        List<ExameDTO> exames = new ArrayList<>();
        exames.add(new ExameDTO(1l,"acv",TipoExame.ANALISE_CLINICA));
        exames.add(new ExameDTO(2l,"cvb",TipoExame.ANALISE_CLINICA));
        exames.add(new ExameDTO(3l,"rhhb",TipoExame.ANALISE_CLINICA));

        String json = objectMapper.writeValueAsString(exames);
        mockMvc.perform(MockMvcRequestBuilders.put("/exame/many").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isAccepted());
    }
}
