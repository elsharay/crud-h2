package com.testedasa.application;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.testedasa.application.controller.ControllerLaboratorio;
import com.testedasa.application.dto.LaboratorioDTO;
import com.testedasa.application.dto.LaboratorioResponseDTO;
import com.testedasa.application.model.Laboratorio;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class RestLaboratorioTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;


    @Test
   public void shouldFecthAll() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/laboratorio/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldFecthByNome() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/laboratorio/exame/teste").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldInsertOn() throws Exception {
        String json = objectMapper.writeValueAsString(new LaboratorioDTO(null,"teste","teste"));
        mockMvc.perform(MockMvcRequestBuilders.post("/laboratorio/").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated());
    }

    public void shouldInsertMany() throws Exception {
        List<LaboratorioDTO> laboratorios = new ArrayList<>();
        laboratorios.add(new LaboratorioDTO(null,"teste","teste"));
        laboratorios.add(new LaboratorioDTO(null,"teste1","teste1"));
        laboratorios.add(new LaboratorioDTO(null,"teste1","teste1"));

        String json = objectMapper.writeValueAsString(laboratorios);
        mockMvc.perform(MockMvcRequestBuilders.post("/laboratorio/").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldUpdate() throws Exception {
        String json = objectMapper.writeValueAsString(new LaboratorioDTO(1l,"dasa","teste"));
        mockMvc.perform(MockMvcRequestBuilders.put("/laboratorio/1").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isAccepted());
    }

    @Test
    public void shouldUpdateMany() throws Exception {
        List<LaboratorioDTO> laboratorios = new ArrayList<>();
        laboratorios.add(new LaboratorioDTO(1l,"acv","dgh"));
        laboratorios.add(new LaboratorioDTO(2l,"cvb","teste1"));
        laboratorios.add(new LaboratorioDTO(3l,"rhhb","teste1"));

        String json = objectMapper.writeValueAsString(laboratorios);
        mockMvc.perform(MockMvcRequestBuilders.put("/laboratorio/many").contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isAccepted());
    }

    @Test
    public void shouldAddExameToLaboratorio() throws Exception {
            mockMvc.perform(MockMvcRequestBuilders.post("/laboratorio/1/exame/3/add").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldRemoveExameFromLaboratorio() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/laboratorio/1/exame/3/remove").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }



}
